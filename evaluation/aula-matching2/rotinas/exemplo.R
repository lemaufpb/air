#??cobalt
library(cobalt)
library(WeightIt)
library(ebal)
library(stargazer)
library(mlogit)


data("lalonde", package = "cobalt")

# Estimação do ATT: binário

# Binário----

# Propensity Score (logit)
W1 = weightit(treat ~ age + educ + nodegree + married + 
                race + re74 + re75,
              data = lalonde,
              method = "ps",
              estimand = "ATT")

b1 = bal.tab(W1, un = TRUE,
             m.threshold=0.1,
             v.threshold=3)
print(b1)

love.plot(W1)

  # Entropia
  W2 = weightit(treat ~ age + educ + nodegree + married + 
                  race + re74 + re75,
                data = lalonde,
                method = "ebal",
                estimand = "ATT")
  
  b2 = bal.tab(W2, un = TRUE,
               m.threshold=0.1,
               v.threshold=3)
  print(b2)
  
  love.plot(W2, stats = c("m", "ks"),
            thresholds = c(m=0.1, ks=0.05),
            sample.names = c("Não pareado", "Pareado"))

summary(W2)

# Cálculo do ATT
att1 = lm(data = lalonde, re78~treat, weights = W1$weights)
att2 = lm(data = lalonde, re78~treat, weights = W2$weights)

summary(att2)

stargazer(att1, att2, type = "text", decimal.mark = ",", digits = 3)

# Discreta----

table(lalonde$race)

# Propensity Score: logit multinomial
W1 = weightit(race ~ age + educ + nodegree + married + 
               re74 + re75,
              data = lalonde,
              method = "ps",
              estimand = "ATE")

# Entropia
W2 = weightit(race ~ age + educ + nodegree + married + 
                re74 + re75,
              data = lalonde,
              method = "ebal",
              estimand = "ATE")


b1 = bal.tab(W1, un = TRUE,
             m.threshold=0.1,
             v.threshold=3)
print(b1)

b2 = bal.tab(W2, un = TRUE,
             m.threshold=0.1,
             v.threshold=3)
print(b2)


lm('mpg ~ hp', data=mtcars)
# Contínua----

# Propensity Score: regressão linear
W1 = weightit(re75 ~ age + race + educ + nodegree + married + re74,
              data = lalonde,
              method = "ps",
              estimand = "ATE")

# Entropia
W2 = weightit(re75 ~ age + race + educ + nodegree + married + 
                re74,
              data = lalonde,
              method = "ebal",
              estimand = "ATE")


b1 = bal.tab(W1, un = TRUE)
print(b1)

b2 = bal.tab(W2, un = TRUE)
print(b2)


