---
title: Regressão Linear
subtitle: Avaliação de Impacto de Políticas Públicas
author: Aléssio Tony Almeida
date: 'UFPB/PPGE/LEMA -- 2020'
---

# Objetivo

Apresentar códigos básicos para estimação de modelos de regressão linear usando R.


# Procedimentos

## Configuração inicial

Caso os pacotes não estejam instalados, digitar `install.packages()` com o nome dos pacotes.

```
# Limpar ambiente
rm(list=ls())

# Pacotes
library(dplyr)
library(car)
library(lmtest)
library(sandwich)
library(stargazer)
library(ggplot2)

setwd("/Volumes/GoogleDrive/Meu Drive/ufpb/ensino/pos-graduacao/Avaliação de Impacto/Aulas/Prática/dados")

```

## Leitura e inspeção dos dos dados

```
# (formato serializado da linguagem R)
dados <- readRDS("censo_demografico2010_sample_1perc.rds")
ta_ufs <- readRDS("ta_ufs.rds")

# Estatísticas sumárias
summary(dados)

# Estrutura das variáveis
str(dados)

```

## Manipulação de dados

```
df <- dados %>%
      select("coduf"=V0001, "peso"=V0010,"rm"=V1004, "setor"=V1006, 
             "pdom"=V0502, "genero"=V0601,"idade"=V6036, "cor"=V0606, 
             "educacao"=V6400, "viveconjuge"=V0637, 
             "rendatrabp"=V6513,"htrabp"=V0653,
             "posocupacao"=V6930, "outsup"=V0632) %>%
      left_join(ta_ufs,by="coduf") %>%
      mutate(
          genero = ifelse(genero==1,"Homem","Mulher"),
          cor = ifelse(cor %in% c(3,5), "Amarela/Vermelha",
                       ifelse(cor==1,"Branca",
                              ifelse(
                                cor==2,"Preta",
                                ifelse(
                                  cor==4,"Parda",
                                  NA
                                )))),
          idade2 = (idade - mean(idade))^2,
          educacao = ifelse(educacao==1,"a) 1-3 anos",
                        ifelse(
                          educacao==2,"b) 4-8 anos",
                          ifelse(
                            educacao==3,"c) 9-11 anos",
                            ifelse(educacao==4,"d) 12+anos",
                                   ifelse(educacao==5,NA,
                                   educacao))))),
          rm = ifelse(rm %in% c(1:42),"Metrópole","Não metrópole"),
          setor = ifelse(setor==1,"Urbana","Rural"),
          salhora = rendatrabp/(htrabp*4),
          logsalhora = log10(salhora)
      ) %>%
      mutate_at(vars(genero,cor,educacao,rm,setor,nomeuf),funs(as.factor)) %>%
      select(logsalhora,salhora,genero,cor,educacao,idade,idade2,
             rm,setor,nomeuf,peso) %>%
      filter(is.finite(logsalhora)) %>%
      na.omit

str(df)
summary(df)


```

## Estatísticas descritivas

### Média salário-hora por faixa de instrução

```

# Média sem controle para o peso amostra
df %>% group_by(educacao) %>% 
  summarise(med = mean(salhora))

# Média com controle para o peso amostra
df %>% group_by(educacao) %>% 
       summarise(med = weighted.mean(salhora,peso))


# Gráfico de barras
df %>% group_by(educacao) %>% 
  summarise(med = weighted.mean(salhora,peso)) %>%
  arrange(desc(educacao)) %>%
  ggplot(aes(educacao,med)) + 
  geom_bar(stat="identity") +
  xlab("Faixa de instrução") + ylab("Salário-hora (média)") 


df %>% group_by(educacao,"Gênero"=genero) %>% 
  summarise(med = weighted.mean(salhora,peso)) %>%
  arrange(desc(educacao)) %>%
  ggplot(aes(educacao,med)) + 
  geom_bar(stat="identity",aes(fill=`Gênero`)) +
  xlab("Faixa de instrução") + ylab("Salário-hora (média)") 

df %>% group_by(educacao,"Gênero"=genero) %>% 
  summarise(med = weighted.mean(salhora,peso)) %>%
  arrange(desc(educacao)) %>%
  ggplot(aes(educacao,med)) + 
  geom_bar(stat="identity",aes(fill=`Gênero`),position="dodge") +
  xlab("Faixa de instrução") + ylab("Salário-hora (média)") +
  theme_classic()

```

## Teste de Diferença de médias
```
t.test(salhora~genero,data=df)
t.test(salhora~educacao,
       data=filter(df,educacao %in% c("a) 1-3 anos","d) 12+anos")))
```

## Modelo de regressão linear

```
r1 <- lm(formula=logsalhora~educacao,weights = peso,data=df)
print(r1)

# objetos retornados pela função lm
names(r1)

# Plotar gŕaficos de diagnósticos
plot(r1)

# Sumarizar resultados
s <- summary(r1)
s

# Objetos retornados
names(s)
```

### Exportação dos resultados
```
stargazer(r1,type="text",digits = 4,summary=FALSE, rownames=FALSE,
          decimal.mark=",",digit.separator=".")


# Acessar objetos
r1$coefficients
coef(r1)

vcov(r1)

confint(r1,level=0.95)

# Predição condicionada
predict(r1, newdata = data.frame(educacao = "d) 12+anos"),
        interval = "confidence")

predict(r1, newdata = data.frame(educacao = "b) 4-8 anos"),
        interval = "confidence")


# Valores preditos
fitted(r1)
predict(r1)
r1$fitted.values

# resíduos
residuals(r1)
r1$residuals

#-------------------------
# Modelo de regressão II
#------------------------

eq <- as.formula(logsalhora ~ genero + cor + idade + idade2 + educacao ) #HABILIDADE
r2 <- lm(formula=eq,data=df,weights = peso)
r2
summary(r2)

# Alterando a categoria base - cor, genero
eq <- as.formula(logsalhora ~ relevel(genero,"Mulher") + relevel(cor,"Branca") + idade + idade2 + educacao )
r2 <- lm(formula=eq,data=df,weights = peso)
r2

# Sumário
summary(r2)

anova(r2)

# Teste de hipóteses lineares sobre coeficientes
linearHypothesis(r2, 'relevel(cor, "Branca")Preta = -0.5')

linearHypothesis(r2, 'idade = 0')

# Testar diferença entre os modelos
anova(r2,r1)
waldtest(r2,r1)
AIC(r1)
AIC(r2)


# Correção para heterocedasticidade
r1.c <- coeftest(r1,  vcov = vcovHC, type="HC0")
r2.c <- coeftest(r2,  vcov = vcovHC, type="HC0")

stargazer(r2,r2.c,type="text",digits = 4,summary=FALSE, rownames=FALSE,
          decimal.mark=",",digit.separator=".",
          column.labels = c("Modelo II","Modelo II (SE robusto)"),
          dep.var.labels = c("logsalhora","logsalhora"))


# Comparar modelos
stargazer(r1.c,r2.c,type="text",digits = 4,summary=FALSE, rownames=FALSE,
          decimal.mark=",",digit.separator=".",
          column.labels = c("Modelo I (SE robusto)","Modelo II (SE robusto)"),
          dep.var.labels = c("logsalhora","logsalhora"))


# Discriminando fatores regionais 
eq <- as.formula(logsalhora ~ relevel(genero,"Mulher") + relevel(cor,"Branca") + idade + idade2 + 
                   educacao + relevel(setor,"Rural") + relevel(rm,"Não metrópole") + relevel(nomeuf,"São Paulo")  )
r3 <- lm(formula=eq,data=df,weights = peso)
r3.c <- coeftest(r3,  vcov = vcovHC, type="HC0")
r3.c

# Comparar modelos
waldtest(r3,r2)
AIC(r2)
AIC(r3)


stargazer(r1.c,r2.c,r3.c,type="text",digits = 4,summary=FALSE, rownames=FALSE,
          decimal.mark=",",digit.separator=".",
          column.labels = c("Modelo I (SE robusto)","Modelo II (SE robusto)","Modelo III (SE robusto)"),
          dep.var.labels = c("logsalhora","logsalhora","logsalhora"))

stargazer(r1.c,r2.c,r3.c,type="text",digits = 4,summary=FALSE, rownames=FALSE,
          decimal.mark=",",digit.separator=".",
          column.labels = c("Modelo I (SE robusto)","Modelo II (SE robusto)","Modelo III (SE robusto)"),
          dep.var.labels = c("logsalhora","logsalhora","logsalhora"),
          single.row=TRUE)
```