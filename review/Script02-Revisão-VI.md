---
title: Regressão com Variáveis Instrumentais
subtitle: Avaliação de Impacto de Políticas Públicas
author: Aléssio Tony Almeida
date: 'UFPB/PPGE/LEMA -- 2020'
---

# Objetivo

Apresentar códigos básicos para estimação de modelos de regressão linear usando R.


# Procedimentos

## Configuração inicial

Caso os pacotes não estejam instalados, digitar `install.packages()` com o nome dos pacotes.

```
# Limpar ambiente
rm(list=ls())

# Pacotes
library(AER)

setwd("/Volumes/GoogleDrive/Meu Drive/ufpb/ensino/pos-graduacao/Avaliação de Impacto/Aulas/Prática/dados")

```

# Declarando Dados
```
y<-as.matrix(c(100,120, 150, 180, 230, 300, 350))
x<-matrix(c(rep(1,7),0,1, 6,5,10,14,16),7,2)
z<-matrix(c(rep(1,7),0,0, 1,0, 1, 1, 1),7,2)
```

# Procedimentos

## Estimando os parâmetros

Estimação usando abordagem matricial
```
beta.ols<- solve((t(x)%*%x)) %*%  (t(x)%*%y)
beta.iv<- solve((t(z)%*%x)) %*%  (t(z)%*%y)
```

Usando pacotes do R
```
# Modelo OLS
summary(lm(y~x[,2]))      

# Modelo IV

# Estágio I: Equação reduzida
summary(lm(x[,2]~z[,2]))
r=lm(x[,2]~z[,2])   
coef(r)
x_hat = fitted(r)

# Estágio II
summary(lm(y~x_hat))        # IV.Instrumento

```

## Usando pacote AER
Note que o modelo estimado em dois estágios geram os mesmos resultados da regressão usado `ivreg`. Contudo, o primeiro método gera estimativas mais ineficientes.
```
summary(ivreg(y~x[,2]|z[,2]),   # IV.Estimacao
        diagnostics = TRUE)	
```

## Usando Estatística de Wald

Aplicado quando o instrumento é uma variável binária.

$$\delta = \dfrac{E(y|z=1)-E(y|z=0)}{E(x|z=1)-E(x|z=0)}$$

```
df = data.frame(y,x,z) %>% 
  select(y, x=X2, z=X2.1)

(with(df, mean(y[z==1])) - with(df, mean(y[z==0])))/(with(df, mean(x[z==1])) - with(df, mean(x[z==0])))

```