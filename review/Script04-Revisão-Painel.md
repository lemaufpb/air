---
title: Dados em Painel
subtitle: Avaliação de Impacto de Políticas Públicas
author: Aléssio Tony Almeida
date: 'UFPB/PPGE/LEMA -- 2020'
---

# Objetivo

Apresentar códigos básicos para estimação de modelos de regressão com dados em painel usando R.

# Sobre a aula

Os exercícios estão baseados em Wooldridge, J. M. Econometric Analysis of Cross Section and Panel Data. Exemplos 10.4 a 10.6.

O escopo é avaliar os efeitos de subsídios concedidos pelo governo para o treinamento de trabalhadores sobre a taxa de perda da produção das firmas. 

Na base JTRAIN (disponível em: 
http://fmwww.bc.edu/ec-p/data/wooldridge/datasets.list.html), temos dados de firmas para os anos de 1987,  1988 e 1989. Os subsídios foram concedidos a partir de 1988 e cada firma pode ter recebido o mesmo em apenas um ano (1988 ou 1989). 

O log da taxa de perda (lscrap) pode ser explicado pelo status da firma em relação à sindicalização (union), efeitos do tempo (dummies d88 e d89), o possível recebimento de subsídio (dummy grant) e o recebimento de subsídio no período passado (dummy grant_1), uma vez que devemos permitir que o efeito do subsídio possa persistir por um período.

- Os dados disponíveis são:
1. year                     1987, 1988, or 1989
2. fcode                    firm code number
3. employ                   # employees at plant
4. sales                    annual sales, $
5. avgsal                   average employee salary
6. scrap                    scrap rate (per 100 items)
7. rework                   rework rate (per 100 items)
8. tothrs                   total hours training
9. union                    =1 if unionized
10. grant                    =1 if received grant
11. d89                      =1 if year = 1989
12. d88                      =1 if year = 1988
13. totrain                  total employees trained
14. hrsemp                   tothrs/totrain
15. lscrap                   log(scrap)
16. lemploy                  log(employ)
17. lsales                   log(sales)
18. lrework                  log(rework)
19. lhrsemp                  log(1 + hrsemp)
20. lscrap_1                 lagged lscrap; missing 1987
21. grant_1                  lagged grant; assumed 0 in 1987
22. clscrap                  lscrap - lscrap_1; year > 1987
23. cgrant                   grant - grant_1
24. clemploy                 lemploy - lemploy[t-1]
25. clsales                  lavgsal - lavgsal[t-1]
26. lavgsal                  log(avgsal)
27. clavgsal                 lavgsal - lavgsal[t-1]
28. cgrant_1                 cgrant[t-1]
29. chrsemp                  hrsemp - hrsemp[t-1]
30. clhrsemp                 lhrsemp - lhrsemp[t-1]

# Procedimentos

## Configuração inicial

Caso os pacotes não estejam instalados, digitar `install.packages()` com o nome dos pacotes.

```
# Limpar ambiente
rm(list=ls())

# Pacotes
library(plm)
library(readstata13)
library(dplyr)
library(AER)
library(stargazer)  
library(tidyr)
library(wooldridge)


```


# Leitura e inspeção dos dados

```
dados = wooldridge::jtrain

# Quantidade de empresas
N=length(unique(dados$fcode))

# Quantidade de períodos
T=length(unique(dados$year))

N * T

# Inspeção Y
table(is.na(dados$lscrap))

df = drop_na(dados, lscrap)

# Quantidade de empresas
N=length(unique(df$fcode))

# Quantidade de períodos
T=length(unique(df$year))

N * T


df_between = group_by(df, fcode) %>%
  summarise_all(list(~mean(., na.rm = T))) %>% 
  ungroup %>% 
  select(-year, fcode, sales, union) 

df_within = select(df, fcode, year, sales) %>% 
  group_by(fcode) %>% 
  mutate(sales_media= mean(sales, na.rm = T),
         sales_desv = sales-sales_media)
```

# Modelos de Painel
## POLS
```
pols1 <- lm(formula= lscrap ~ union + d88 + d89 + grant + grant_1, data=dados)

pols <- plm(formula= lscrap ~ union + d88 + d89 + grant + grant_1, data=dados, index = c("fcode","year"),model = "pooling")

summary(pols,vcov=vcovHC)

```

## LSDV
```
lsdv <- lm(formula= lscrap ~ union + d88 + d89 + grant + grant_1 +
             I(as.factor(fcode)), data=dados)
```
## Efeito Fixo (Within)
```
within <- plm(formula= lscrap ~ union + d88 + d89 + grant + grant_1,
               index = c("fcode","year"), model = "within", data=dados)

summary(within,vcov=vcovHC)               

```

### Análises da heterogeneidade individual
```
# Estimativas de  efeito-fixo por indivíduo
summary(fixef(within))

hist(fixef(within),breaks = 50)

plot(density(fixef(within)))

```

## Efeito aleatório
```
random <- plm(lscrap ~ union + d88 + d89 + grant + grant_1,data=dados, index = c("fcode","year"),model = "random")

summary(random,vcov=vcovHC)
```


## Hausman test
```
phtest(within,random)
```

## Primeiras diferenças
```
fd <- plm(formula= lscrap ~ union + d88 + d89 + grant + grant_1,
              index = c("fcode","year"), model = "fd", data=dados)
```

# Comparação dos resultados
```
stargazer(pols, lsdv, within, fd, random, keep = c("union", "grant", "grant_1"), type = "text")
```

# Outras especificações
## FE - twoways => individual and time effects
```
FE <- plm(lscrap ~ union + d88 + d89 + grant + grant_1,data=dados,
          index = c("fcode","year"), model = "within", effect = "twoways")

summary(FE)          

summary(fixef(FE, effect = "time"))
summary(fixef(FE, effect = "individual"))
```


## RE - twoways => individual and time effects
```
RE <- plm(lscrap ~ union + d88 + d89 + grant + grant_1,data=dados,
          index = c("fcode","year"), model = "random", effect = "twoways")

summary(RE,vcov=vcovHC)
```
